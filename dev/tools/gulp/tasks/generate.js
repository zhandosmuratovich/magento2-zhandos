const context = require('../context'),
  tap = require('gulp-tap'),
  path = require('path'),
  fs = require('fs'),
  extraFs = require('fs-extra'),
  gulp = require('gulp'),
  _ = require('lodash');

let getFileDetails = (file) => {
  let regex = new RegExp('^(.*)\/(?:.*\/)?(.*)'),
    capture = regex.exec(file);
  
  return {
    type: capture[1],
    name: capture[2],
  };
};

/**
 * Generate web assets
 *
 * @param files
 * @param dest
 */
let generateAssets = (files, dest) => {
  _.forEach(files, (file) => {
    let fileDetails = getFileDetails(file);
    fileDetails.type = fileDetails.type === 'css' ? 'scss' : fileDetails.type;
    let filePath = path.resolve(dest, 'web', 'assets', fileDetails.type, fileDetails.name + '.' + fileDetails.type);
    extraFs.outputFile(filePath, '', (err) => {
      if (err) {
        console.error(err);
      }
    });
  });
};

/**
 * Generate default etc/view.xml file that is mandatory
 * for new M2 theme without parent theme set
 *
 * @param theme
 * @param dest
 */
let generateDefaultView = (theme, dest) => {
  if (!theme.parent) {
    let viewSrc = path.resolve(context.libDir, 'templates/etc', 'view.xml'),
      destPath = path.resolve(dest, 'etc', 'view.xml');
    extraFs.outputFile(destPath, fs.readFileSync(viewSrc, 'utf8'), (err) => {
      if (err) {
        console.error(err);
      }
    });
  }
};

/**
 * Process file by replacing placeholders onto config values
 *
 * @param file
 * @param theme
 */
let processFile = (file, theme) => {
  let ext = path.extname(file.path),
    fileContents = file.contents.toString(),
    replace = '';
  
  switch (ext) {
    case '.php':
      replace = fileContents.replace('{{componentName}}', theme.area + '/' + theme.name);
      break;
    case '.xml':
      replace = fileContents.replace('{{title}}', theme.name);
      
      /**
       * If 'parent' config node is not set, remove it from .xml generation
       *
       * @type {string}
       */
      if (theme.parent) {
        replace = replace.replace('{{parent}}', theme.parent);
      } else {
        replace = replace.replace(/.*{{parent}}.*/, '');
        replace = replace.replace(/^\s*$(?:\r\n?|\n)/gm, '');
      }
      break;
    default:
      console.error(`File type ${ext} is not supported`);
      break;
  }
  
  if (replace) {
    file.contents = new Buffer(replace);
  }
};

module.exports = () => {
  _.forEach(context.themes, (theme) => {
    let themeDir = path.resolve(context.magentoRoot, 'app/design', theme.area, theme.name);
    
    if (fs.existsSync(themeDir)) {
      console.info(`${theme.name} theme already exists`);
      return;
    }
    
    let src = path.resolve(context.libDir, 'templates', '*.*'),
      dest = path.resolve(context.magentoRoot, 'app/design', theme.area, theme.name);
    generateAssets(theme.files, dest);
    generateDefaultView(theme, dest);
    
    return gulp.src(src)
      .pipe(tap((file) => {
        processFile(file, theme);
      }))
      .pipe(gulp.dest(dest));
  });
};
