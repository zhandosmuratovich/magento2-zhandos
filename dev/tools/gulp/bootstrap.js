'use strict';

const gulp = require('gulp'),
  context = require('./context'),
  sequence = require('run-sequence'),
  args = require('minimist')(process.argv.slice(2));

/**
 * Add multiple options to define environment.
 *
 * Fallback to system ENV VAR first, before apply default value (development).
 */
process.env.NODE_ENV = args.production ? 'production'
  : process.env.NODE_ENV || args.env || 'development';

module.exports = () => {
  context.config.tasks.list.forEach(function (task) {
    gulp.task(task, require(`${context.libDir}/tasks/${task}`));
  });
  
  gulp.task('default', function (cb) {
    sequence(...context.config.tasks.default.concat([cb]));
  });
  
  gulp.task('watch', [], require(`${context.libDir}/tasks/watch`));
  
  return gulp;
};
